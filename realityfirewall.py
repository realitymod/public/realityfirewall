import host
import realitytimer as rtimer
import realitylogger as rlogger
import realitydebug as rdebug
import realitymemory as rmemory
import time


def init():
    global _realityfirewall
    if rmemory.isWindowsListenServer:
        return


    import _realityfirewall

    timer = rtimer.Timer(refresh, 1.0, 1)
    timer.setRecurring(1.0)

    rlogger.createLogger("firewall", "admin/logs",  "firewall.log", True)


lastPacketCount = 0
isOn = False
def refresh(args=None):
    global lastPacketCount, isOn

    count = _realityfirewall.getPacketCount()
    rate = count - lastPacketCount
    lastPacketCount = count

    logAndPrint("rate: %s, total dropped: %s" % (rate, _realityfirewall.getDroppedCount()))


    if not isOn:
        if rate > 20000:
            _realityfirewall.setProtected(True)
            logAndPrint("FW on")
            isOn = True
    else:
        if rate < 20000:
            _realityfirewall.setProtected(False)
            logAndPrint("FW off")
            isOn = False


def logAndPrint(msg):
    rlogger.RealityLogger["firewall"].logLine( time.strftime("%Y-%m-%d %H:%M:%S ") + msg)
    rdebug.debugMessage(msg)