#define NOMINMAX

#include "realityfirewall.hpp"
#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <mutex>

// Windows crap
#include <Ws2tcpip.h>
#include <winsock2.h>
#pragma comment(lib, "Ws2_32.lib")


namespace py = pybind11;
py::object host;
py::object rcon_invoke;




// Main thread RW, recv thread read. Non-atomic
std::map<unsigned int, int> clients; // bag of IPs
std::mutex lock;


// Recv thread atomic write, main thread R
unsigned long long packets = 0;
unsigned long long droppedpackets = 0;
bool protect = false;


void sayAll(std::string str)
{
    py::object res = rcon_invoke.call(py::bytes("game.sayall \"" + str + "\""));
}

void printConsole(std::string str)
{
    py::object res = rcon_invoke.call(py::bytes("echo \"" + str + "\""));
}


// Main thread
void onPlayerConnect(py::object player)
{
    py::str pyaddr = player.attr("getAddress").call();
    std::string addrs = pyaddr;
    unsigned int addr = 0;
    inet_pton(AF_INET, addrs.c_str(), &addr);


    lock.lock();
    if (clients.count(addr) == 0)
        clients[addr] = 1;
    else
        clients[addr] += 1;

    lock.unlock();
}

// Main thread
void onPlayerDisconnect(py::object player)
{
    py::str pyaddr = player.attr("getAddress").call();
    std::string addrs = pyaddr;
    unsigned int addr = 0;
    inet_pton(AF_INET, addrs.c_str(), &addr);

    if (clients.count(addr) == 0)
        return;
    
        
    lock.lock();
    if (clients[addr] == 1)
        clients.erase(addr);
    else
        clients[addr] -= 1;

    lock.unlock();
}

// Main thread
unsigned long long getPacketCount() {
    return packets;
}

// Main thread
unsigned long long getDroppedCount() {
    return droppedpackets;
}

// Main thread
void setProtected(py::bool_ status) {
    protect = status;
}



//// main thread
//void checkPacketRate(py::object args) {
//
//    int count = packets - lastpackets;
//    if (!protect)
//    {
//        if (count > 10000) {
//            protect = true;
//            sayAll("FW on");
//            printConsole("FW on");
//        }
//    }
//    else {
//        if (count < 10000)
//        {
//            protect = false;
//            sayAll("FW off");
//            printConsole("FW off");
//        }
//     }
//
//    char buf[0x100];
//
//    sprintf(buf, "packet rate: %d, total dropped: %d", count, droppedpackets);
//    printConsole(std::string(buf));
//
//    lastpackets = packets;
//}


// Recv thread
int recv_hook(SOCKET   s,
    char* buf,
    int      len,
    int      flags,
    sockaddr* from,
    int* fromlens) {
    
    int out;
    while (true)
    {
        
        out = recvfrom(s, buf, len, flags, from, fromlens);


        struct sockaddr_in* addr_in = (struct sockaddr_in*)from;
        unsigned int addr = addr_in->sin_addr.S_un.S_addr;        

        if (out < 0)
            return out;

        packets++;
        if (protect) {
            lock.lock();
            if (clients.count(addr) == 0) {
                lock.unlock();
                droppedpackets++;          
                continue; // DROP
            }
            lock.unlock();
        }


        return out;
    }
}


bool overwriteCall(void* callPosition, void* newFunction)
{
    char* _callPosition = (char*)callPosition;
    DWORD oldProtect;
    DWORD newProtect;  // "If this parameter is NULL the function fails." thanks
                       // windows

    unsigned int offset =
        (unsigned int)(newFunction)-((unsigned int)callPosition + 5);
    VirtualProtect(_callPosition + 1, 4, 0x40, &oldProtect);
    _callPosition[0] = 0xE8;
    _callPosition[5] = 0x90;
    memcpy(_callPosition + 1, &offset, 4);
    return VirtualProtect(_callPosition + 1, 4, oldProtect, &newProtect);
}


PYBIND11_MODULE(_realityfirewall, m) {


	host = py::module::import("host");
	rcon_invoke = host.attr("rcon_invoke");

    py::object registerHandler = host.attr("registerHandler");
    registerHandler(py::bytes("PlayerConnect"),
        std::function<void(py::object)>(&onPlayerConnect), 1);
    registerHandler(py::bytes("PlayerDisconnect"),
        std::function<void(py::object)>(&onPlayerDisconnect), 1);
	

    m.def("getPacketCount", &getPacketCount, R"pbdoc()pbdoc");
    m.def("getDroppedCount", &getDroppedCount, R"pbdoc()pbdoc");
    m.def("setProtected", &setProtected, R"pbdoc()pbdoc");


    //py::object rtimer = py::module::import("game.realitytimer");
    //py::object timer = rtimer.attr("Timer")(std::function<void(py::object)>(&checkPacketRate), 1.0f, 1);
    //timer.attr("setRecurring")(1.0f);


    overwriteCall((void*)0x00667A61, recv_hook);


#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif






}

